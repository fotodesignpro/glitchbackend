from fastapi import FastAPI, Body, Header, File, Depends, HTTPException, APIRouter, Request 
from models.users import Users
from models.tx import Tx 

from starlette.status import HTTP_201_CREATED, HTTP_401_UNAUTHORIZED
from starlette.responses import Response
from utils.debug import dprint, pprint
from utils.db_functions import db_check_user, db_post, db_select, db_update, db_post_delete
from utils.mesages import MS_UNAUTHORIZED, MS_TOKEN_DESC, MS_TOKEN_SUMMARY, USER_AUTH_FAILED
from utils.web3_functions import verify_signed_message, verif_contract_hash, verif_address_hash
from utils.security import current_user, get_hashed_password
from typing import Dict
import os
import demjson
import json

app_v1  = APIRouter()

@app_v1.patch("/update")
async def update(sign_dict:Dict):
   user_addr = await verify_signed_message(sign_dict) 
   result = await db_select(tname='users', sel='*', col='address', con='where', val=user_addr)
   if type(result) is dict:
        return result
        result = await db_update(user, None)
        return result 


@app_v1.post("/bundel")
async def update(sign_dict:Dict):
   user_addr = await verify_signed_message(sign_dict) 
   result = await db_select(tname='users', sel='*', col='address', con='where', val=user_addr)

   if type(result) is dict:

       tx_dict = json.loads(sign_dict["text"])     
       str_dict = list(tx_dict.values())
       org_dict = list(sign_dict["bundel"].values())

       # compares the string dict with the orginal dict
       # becaus json.stringify is not perfect
       if str_dict == org_dict:
            print("let's save it")
            print(sign_dict["bundel"])
            result = await db_post(sign_dict["bundel"], user_addr)
            # will return true or false
            print(result)
            if result:
                return result
            else:
                return result
       else:
            return "not equel"
   else:    
       return "false"


@app_v1.post("/tx")
async def post_tx(tx_dict:Tx):
   #user_addr = await verify_signed_message(sign_dict) 
   #result = await db_select(tname='users', sel='*', col='address', con='where', val=user_addr)
   #print(result)
   #tx_dict["tname"] = "tx"
   #tx_dict["verified"] = False

   #return update_count_dict 
   #return result 
   #print(tx_dict.hash) 
   new_tx_dict = await verif_address_hash(tx_dict) 
   new_tx_dict["tname"] = "tx"
   new_tx_dict["verified"] = False

   # adds transaction details to the Database
   result = await db_post(new_tx_dict, new_tx_dict["user_addr"])

   # increments the user_count (user that payed to the bundel)
   if result is True: 
      bundel = await db_select(tname='bundels', sel='user_count', col='id', con='where', val=new_tx_dict["bundel_id"])
      
      if bundel["user_count"] is None:
          inc_user_count = 1
      else:
          inc_user_count = int(bundel["user_count"]) + 1

      update_count_dict = {
        "tname": "bundels",
        "id": new_tx_dict["bundel_id"],
        "user_count": inc_user_count 
      }   
      result = await db_update(update_count_dict, None)
      return result
      return {'result': 'saved'}
      
   if result == "bundels":
      return {'result': "user not exists"}
   else:
      return {'result': 'In error happend'}
   #return "false"
 

@app_v1.get("/check_addr/{addr}")
async def get_bundels(addr:str):
    role = await db_select(tname='users', sel='role', col='address', con='where', val=addr.lower() )
    if type(role) is dict:
        if role["role"] == "admin":
            return True
        else:
            return False
    else:
        return False

@app_v1.get("/bundels")
async def get_bundels():

    result = await db_select(tname='bundels', sel="id, creator, goal, image_addr, image_url, image_id, platform, users, value, addr_list, buy_sell_tx, open, user_count, sign" , row=False)
    
    userlist = []
    for index, bundel in enumerate(result):
        print("###############################################")
        print("index ", index)
        tx = await db_select(tname='tx', sel='*', col='bundel_id', con='where', val=bundel["id"], row=False) 
        print(tx)
        """
        for t in tx: 
            if t["bundel_id"] == bundel["id"]: 
                print("----------")
                print(t["bundel"])
                userlist.append(d["user_addr"]) 
                bundel["addr_list"] = userlist
          
        """ 
    return result  #json.loads(result[0]["bundel"])
    
@app_v1.get("/tx")
async def get_bundels():

    result = await db_select(tname='tx', sel="*" , row=False)
    print("-------------")
    print(result)
    return result  #json.loads(result[0]["bundel"])

@app_v1.post("/login", description=MS_TOKEN_DESC, summary=MS_TOKEN_SUMMARY)
async def login(user:Users):

    user_dict = await db_select(tname='users', sel='*', col='address', con='where', val=user.address)
    if user_dict is None:

        new_user_dict = user.dict(exclude_unset=True)
        new_user_dict["tname"] = "users"
        print(new_user_dict)
        result = await db_post(new_user_dict, user_addr=None)
        if result is True:
            return {'result': 'saved'}
        if result == "username":
            return {'result': "username already exists"}
        if result == "email":
            return {'result': "email already exists"}
        else:
            return {'result': 'In error happend'}
    else:
        return "user already exists"
