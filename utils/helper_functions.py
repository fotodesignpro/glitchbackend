import requests
#from utils.const import UPLOAD_PHOTO_URL
import os
import uuid
import base64
import json
import shutil
from utils.debug import pprint, dprint
from fastapi.responses import FileResponse  
dirname = os.path.abspath(os.curdir)
img_path = dirname + "/img/"

def detect_mime_type(base64):
    signatures = {
    "JVBERi0": ["application/pdf", "pdf" ],
    "R0lGODdh": ["image/gif", "gif"],
    "R0lGODlh": ["image/gif", "gif"],
    "iVBORw0KGgo": ["image/png", "png"],
    "/9j/": ["image/jpg", "jpg"]
    }
    for s in signatures:
        if(base64.find(s)!=-1):
            return signatures[s][1]

async def save_img(images, full_path):
    print("save img")
    img_arr = [] 
    for image in images:

        # create image name
        name = uuid.uuid4().hex

        # get image extension
        extension = detect_mime_type(image)
    
        # image full path
        image_location = f"{full_path}{name}.{ extension }" 

        # save image to the server
        with open(image_location, "wb") as file:
            image = image.partition(",")[2]
            file.write(base64.b64decode(image))
            img_arr.append(f"{name}.{extension}")

    return img_arr

async def upload_image(obj):
    #result = request.post(UPLOAD_PHOTO_URL, file=file)
    images = obj["images"]

    full_path = img_path + obj["tname"] + "/"

    # path will be created if it's doesnt exist.
    if not os.path.isdir(full_path):
        os.makedirs(full_path)

    if images:
        result = await save_img(images, full_path)
        return result 

async def update_image(obj, old_img ):
        print("update image")
        full_path = img_path + obj["tname"] + "/"

        if obj["images"] != old_img["images"] :
            # if images in Datatase is emty and and it gets new images 
            # from fronend
            if obj["images"] is not None and old_img["images"] is not None :
                # sort the array elemnts with set
                new_img_arr = set(obj["images"]) 
                old_img_arr = set(old_img["images"])
                # compare the old image array with the new image array
                # to find out with image have bin changed and save the result in a array
                remove_img_arr = list(old_img_arr.difference(new_img_arr))
                save_base64_arr = list(new_img_arr.difference(old_img_arr))
                keep_img_arr = list(set(old_img["images"]) - set(remove_img_arr))
                print("both images are there")
            else:
                # if there is no images in the database but new images gets added 
                if obj["images"] is not None:
                    print("old_images is none")
                    print(obj["images"])
                    save_base64_arr = obj["images"]
                    remove_img_arr = []
                    keep_img_arr = []
                # if images get remove but no new images gets added 
                if old_img["images"] is not None: 
                    print("new images is none")
                    save_base64_arr = []
                    remove_img_arr = obj["images"]
                    keep_img_arr = list(set(old_img["images"]) - set(remove_img_arr))

            if len(remove_img_arr) > 0:
                # this for loop removs images
                for image in remove_img_arr:
                 path = os.path.exists(full_path + '/' + image)
                 if path: 
                     os.remove(full_path + image ) 
                 if path is False:
                     print(f"image {image} has bin remove")
                  
            if len(save_base64_arr) > 0:
                result = await save_img(save_base64_arr, full_path)
                keep_img_arr = keep_img_arr + result 

            print("+++++++++++++++ collectdata ++++++++++++++++++") 
            print(remove_img_arr) #two remove
            print(save_base64_arr) #to add
            print(keep_img_arr) # keep 

            return keep_img_arr
         
        else: 
            return obj["images"]
    

async def delete_image(obj):
    print("delete image")
    if obj["images"] is not None:
        full_path = img_path + obj["tname"] + "/"
        remove_img_arr = obj["images"] 
        if len(remove_img_arr) > 0:
            # this for loop removs images
            for image in remove_img_arr:
             path = os.path.exists(full_path + '/' + image)
             if path: 
                 os.remove(full_path + image ) 
             if path is False:
                 print(f"image {image} has bin remove")



